package com.ezefm.futapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FutappApplication {

	public static void main(String[] args) {
		SpringApplication.run(FutappApplication.class, args);
	}

}
