package com.ezefm.futapp.controller;

import com.ezefm.futapp.entity.Coach;
import com.ezefm.futapp.exception.NullFieldException;
import com.ezefm.futapp.service.CoachService;
import com.ezefm.futapp.service.GenericMethods;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/coach")
public class CoachController {
    private final GenericMethods<Coach> genericMethods;
    private final CoachService coachService;
    public CoachController(GenericMethods<Coach> genericMethods, CoachService coachService) {
        this.genericMethods = genericMethods;
        this.coachService = coachService;
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Coach>> getAll(){
        return new ResponseEntity<>(genericMethods.getAll(), HttpStatus.OK);
    }

    @PostMapping("/create")
    public ResponseEntity<Coach> newCoach(@RequestBody Coach coach) {
        Coach response = genericMethods.create(coach);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }



    // Exception Handlers //
    @ExceptionHandler(NullFieldException.class)
    public ResponseEntity<String> handleTeamNullException(NullFieldException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
    }

}