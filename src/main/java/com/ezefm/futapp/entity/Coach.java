package com.ezefm.futapp.entity;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@NoArgsConstructor
@Data
public class Coach extends Member {
    private Integer experience;

    public Coach(String name, String lastName, Integer age, String team, Integer experience) {
        super(name, lastName, age, team);
        this.experience = experience;
    }
}