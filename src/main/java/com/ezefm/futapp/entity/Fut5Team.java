package com.ezefm.futapp.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;

import java.time.LocalDate;
import java.util.List;

@Entity
public class Fut5Team {

    @Id
    private Long id;
    private String name;
    private LocalDate creationDate;
    @OneToMany
    private List<Player> players;
    @OneToOne
    private Coach coach;

    public Fut5Team() {}

    public Fut5Team(Long id, String name, LocalDate creationDate, List<Player> players, Coach coach) {
        this.id = id;
        this.name = name;
        this.creationDate = creationDate;
        this.players = players;
        this.coach = coach;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Coach getCoach() {
        return coach;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }
}