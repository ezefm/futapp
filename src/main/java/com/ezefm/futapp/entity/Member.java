package com.ezefm.futapp.entity;

import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.*;

import java.util.UUID;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class Member {

    @Id
    protected UUID id = UUID.randomUUID();
    protected String name;
    protected String lastName;
    protected Integer age;
    protected String team;

    public Member(String name, String lastName, Integer age, String team) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.team = team;
    }
}