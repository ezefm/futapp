package com.ezefm.futapp.entity;

import jakarta.persistence.Entity;

@Entity
public class Player extends Member{
    private Position position;
    private Integer goals;
    private Integer matchesPlayed;
    private Boolean isCaptain;
    private Integer tShirtNumber;

    public Player() {
    }

    public Player(String name, String lastName, Integer age, String team, Position position, Integer goals, Integer matchesPlayed, Boolean isCaptain, Integer tShirtNumber) {
        super(name, lastName, age, team);
        this.position = position;
        this.goals = goals;
        this.matchesPlayed = matchesPlayed;
        this.isCaptain = isCaptain;
        this.tShirtNumber = tShirtNumber;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getGoals() {
        return goals;
    }

    public void setGoals(Integer goals) {
        this.goals = goals;
    }

    public Integer getMatchesPlayed() {
        return matchesPlayed;
    }

    public void setMatchesPlayed(Integer matchesPlayed) {
        this.matchesPlayed = matchesPlayed;
    }

    public Boolean getCaptain() {
        return isCaptain;
    }

    public void setCaptain(Boolean captain) {
        isCaptain = captain;
    }

    public Integer gettShirtNumber() {
        return tShirtNumber;
    }

    public void settShirtNumber(Integer tShirtNumber) {
        this.tShirtNumber = tShirtNumber;
    }
}