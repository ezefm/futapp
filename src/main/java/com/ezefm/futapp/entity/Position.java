package com.ezefm.futapp.entity;

public enum Position {
    GOALKEEPER, DEFENCE, A_MIDFIELDER, A_MIDDLE_FORWARD, A_FORWARD

}