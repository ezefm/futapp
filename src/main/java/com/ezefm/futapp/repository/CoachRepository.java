package com.ezefm.futapp.repository;

import com.ezefm.futapp.entity.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface CoachRepository extends JpaRepository<Coach, UUID> {


}