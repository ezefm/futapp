package com.ezefm.futapp.repository;

import com.ezefm.futapp.entity.Fut5Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Fut5TeamRepository extends JpaRepository<Fut5Team, Long> {


}