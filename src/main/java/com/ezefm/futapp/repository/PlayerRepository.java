package com.ezefm.futapp.repository;

import com.ezefm.futapp.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PlayerRepository extends JpaRepository<Player, UUID> {


}