package com.ezefm.futapp.service;

import java.util.List;
import java.util.UUID;

public interface GenericMethods<T> {
    List<T> getAll();
    T create(T entity);
    T read(UUID id) throws Exception;
    T update(UUID id, T entity) throws Exception;
    void delete(UUID id) throws Exception;
}