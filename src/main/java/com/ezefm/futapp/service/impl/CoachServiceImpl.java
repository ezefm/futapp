package com.ezefm.futapp.service.impl;

import com.ezefm.futapp.entity.Coach;
import com.ezefm.futapp.exception.NullFieldException;
import com.ezefm.futapp.repository.CoachRepository;
import com.ezefm.futapp.service.CoachService;
import com.ezefm.futapp.service.GenericMethods;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CoachServiceImpl implements GenericMethods, CoachService {

    private final CoachRepository coachRepository;

    public CoachServiceImpl(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    private void validation(Object object) throws NullPointerException {
        if(object instanceof Coach coach){
            if(coach.getAge() == null){
                throw new NullPointerException("Age cannot be null");
            }
            if(coach.getTeam() == null || coach.getTeam().isEmpty()){
                throw new NullFieldException("Team cannot be null");
            }
            if(coach.getName() == null || coach.getName().isEmpty()){
                throw new NullPointerException("Coach name cannot be null");
            }
            if(coach.getLastName() == null || coach.getLastName().isEmpty()){
                throw new NullPointerException("Coach last name cannot be null");
            }
        }
    }

    @Override
    public List<Coach> getAll(){
        return coachRepository.findAll();
    }

    @Override
    public Object create(Object entity) throws NullPointerException{
//        String name, String lastName, Integer age, String team, Integer experience
        validation(entity);
        Coach coach1 = (Coach) entity;
        Coach coach = new Coach(coach1.getName(),
                coach1.getLastName(),
                coach1.getAge(),
                coach1.getTeam(),
                coach1.getExperience());
        return coachRepository.save(coach);
    }

    @Override
    public Coach read(UUID id) throws Exception {
        Optional<Coach> coach = coachRepository.findById(UUID.fromString(id.toString()));
        if(coach.isPresent()){
            return coach.get();
        } else {
            throw new Exception("Id doesn't exist");
        }
    }

    private Optional<Coach> updateValidation(UUID id, Coach entity) {
        Optional<Coach> coach = coachRepository.findById(id);
        coach.ifPresent( c -> {
            if(entity.getName() != null){
                c.setName(entity.getName());
            }
            if(entity.getLastName() != null){
                c.setLastName(entity.getLastName());
            }
            if(entity.getAge() != null){
                c.setAge(entity.getAge());
            }
            if(!entity.getTeam().isEmpty()){
                c.setTeam(entity.getTeam());
            }
        });
        return coach;
    }

    @Override
    public Coach update(UUID id, Object entity) throws Exception {
        if(updateValidation(id, (Coach) entity).isPresent()){
            Coach updatedCoach = updateValidation(id, (Coach) entity).get();
            return coachRepository.save(updatedCoach);
        } else {
            throw new Exception("Id doesn't exist");
        }
    }

    @Override
    public void delete(UUID id) throws Exception {
        Optional<Coach> optionalCoach = coachRepository.findById(id);
        if(optionalCoach.isPresent()){
            Coach coach = optionalCoach.get();
            coachRepository.delete(coach);
        } else {
            throw new Exception("Coach id doesn't exist");
        }
    }

}